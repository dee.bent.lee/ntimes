import { Action, Reducer } from 'redux';

// -----------------
// STATE - This defines the type of data maintained in the Redux store.

export interface TextBoxState {
    text: string;
}

// -----------------
// ACTIONS - These are serializable (hence replayable) descriptions of state transitions.
// They do not themselves have any side-effects; they just describe something that is going to happen.
// Use @typeName and isActionType for type detection that works even after serialization/deserialization.

interface SetStringAction { type: 'SET_STRING', newString : string }
interface ResetStringAction { type: 'RESET_STRING' }

// Declare a 'discriminated union' type. This guarantees that all references to 'type' properties contain one of the
// declared type strings (and not any other arbitrary string).
type KnownAction = SetStringAction | ResetStringAction;

// ----------------
// ACTION CREATORS - These are functions exposed to UI components that will trigger a state transition.
// They don't directly mutate state, but they can have external side-effects (such as loading data).

export const actionCreators = {
    setString: ( newString : string ) => <SetStringAction>{ type: 'SET_STRING', newString }, //ok that's pretty crazy that this string was checked at compile time lol
    resetString: () => <ResetStringAction>{ type: 'RESET_STRING' }
};

// ----------------
// REDUCER - For a given state and action, returns the new state. To support time travel, this must not mutate the old state.

export const reducer: Reducer<TextBoxState> = (state: TextBoxState, action: KnownAction) => {
    if (action.type === 'SET_STRING') {
        return { text : action.newString } 
    }
    else if (action.type === 'RESET_STRING') {
        return { text : "" };
    }
    else {
        let check : never = action;
    }    

    // For unrecognized actions (or in cases where actions have no effect), must return the existing state
    //  (or default initial state if none was supplied)
    return state || { text: "" };
};
