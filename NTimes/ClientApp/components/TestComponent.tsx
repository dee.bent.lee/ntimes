import * as React from 'react';
import { Link, RouteComponentProps } from 'react-router-dom';
import { connect } from 'react-redux';
import { ApplicationState }  from '../store';
import * as TextBoxStore from '../store/TextBox';

//i guess react combines this stuff together using vudu magic nonsense
type TextBoxProps =
    TextBoxStore.TextBoxState
    & typeof TextBoxStore.actionCreators;
  //  & RouteComponentProps<{}>;

class TextBoxThing extends React.Component<TextBoxProps, {}> {
    input = "";

    inputChanged = (e: React.FormEvent<HTMLInputElement>): void => {
        this.input = e.currentTarget.value;
    }   

    public render() {
        return <div>
            <p>This is a simple example of a React component by Doug.</p>
            <p>It probably is terrible but there you go lol</p>

            <p>Current text: <strong>{ this.props.text }</strong></p>

            <input type="text" onChange={ this.inputChanged }  ></input>
            <button onClick={ () => { this.props.setString(this.input) } }>Settington</button>
            <button onClick={ () => { this.props.resetString() } }>Resettington</button>
        </div>;
    }
}

// Wire up the React component to the Redux store
export default connect(
    (state: ApplicationState) => state.textBoxThing, // Selects which state properties are merged into the component's props
    TextBoxStore.actionCreators            // Selects which action creators are merged into the component's props
)(TextBoxThing) as typeof TextBoxThing;